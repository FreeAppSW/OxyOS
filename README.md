# OxyOS
OxyOS v1.0b6 is a simple and user-friendly virtual desktop environment made in VB.NET.<br>
We feature simple and intuitive UI, lots of featured inbuilt apps and planning a high level of third-party application support.


## Acknowledgements
Most icons used in this product are from http://icons8.com unless stated
