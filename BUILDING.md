# Building OxyOS

If you want to build OxyOS yourself, you should follow this guidelines.

## Work directory
Make sure to have the following directory structure:
```
OxyOS
--UserDocs\
--OS_Data\
----current_app.ostring
```
